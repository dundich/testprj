import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Axios from 'axios'
import ComponentStore from './components/ComponentStore'
import Aitify from './Plugin.js'

Vue.prototype.$http = Axios;

Vue.use(Vuetify, { theme: {  
  secondary: "#1976D2",
  accent: "#03A9F4",
  error: "#f44336",
  warning: "#ffeb3b",
  info: "#2196f3",
  success: "#4caf50"
}})

Vue.use(ComponentStore);
Vue.use(Aitify);

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
